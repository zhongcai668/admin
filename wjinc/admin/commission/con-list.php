<?php
$sql="select uid,username from ssc_members where  `type`=1 and isDelete=0 ";
isset($_REQUEST['username']) && $_REQUEST['username'] && $sql .= " AND username LIKE '%{$_REQUEST['username']}%'";
$data=$this->getPage($sql, $this->page, $this->pageSize);
?>
<article class="module width_full">
    <input type="hidden" value="<?=$this->user['username']?>" />
    <header>
        <h3 class="tabs_involved">佣金列表
            <div class="submit_link wz">
                <form action="/admin778899.php/commission/conCommissionList" target="ajax" call="defaultSearch" dataType="html">
                    会员<input type="text" class="alt_btn" name="username" style="width:90px;"/>&nbsp;&nbsp;
                    时间从 <input type="date" class="alt_btn" style="width:120px;" value="<?=isset($_REQUEST['fromTime'])?$_REQUEST['fromTime']:'' ?>" name="fromTime"/>
                    到 <input type="date" style="width:120px;" name="toTime" value="<?=isset($_REQUEST['toTime'])?$_REQUEST['toTime']:'' ?>" class="alt_btn"/>&nbsp;&nbsp;
                    <input type="submit" value="查找" class="alt_btn">
                    <input type="reset" value="重置条件">
                </form>
            </div>
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <th>ID</th>
            <th>用户名</th>
            <th>下级人数</th>
            <th>总充值</th>
            <th>总取款</th>
            <th>现余额</th>
            <th>盈利额</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody id="nav01">
		<?php
        $total_user = 0;
        $total_recharge = 0;
        $total_cash = 0;
        $total_coin = 0;
        $total_profit = 0;
		if($data['data']) foreach($data['data'] as $var){
			$agent = [];
			$agent = $this->getAgentData($var['uid'],
                isset($_REQUEST['fromTime'])?$_REQUEST['fromTime']:'',
                isset($_REQUEST['toTime'])?$_REQUEST['toTime']:'');
			$profit = round($agent['total_cash'] + $agent['total_coin'] - $agent['total_recharge'], 2);
			$total_user += $agent['total_user'];
			$total_recharge += $agent['total_recharge'];
			$total_cash += $agent['total_cash'];
			$total_coin += $agent['total_coin'];
			$total_profit += $profit;
			?>
                <tr>
                    <td><?=$var['uid']?></td>
                    <td><?=$var['username']?></td>
                    <td><?=$agent['total_user']?></td>
                    <td><?=$agent['total_recharge']?></td>
                    <td><?=$agent['total_cash']?></td>
                    <td><?=$agent['total_coin']?></td>
                    <td><?=$profit?></td>
                    <th><a href="Member/index?type=2&uid=<?=$var['uid']?>" call="">下级详情</a></th>
                </tr>
			<?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <td>--</td>
            <td>--</td>
            <td><?=$total_user?></td>
            <td><?=$total_recharge?></td>
            <td><?=$total_cash?></td>
            <td><?=$total_coin?></td>
            <td><?=$total_profit?></td>
            <th><a href="Member/index?type=2&uid=<?=$var['uid']?>" call="">下级详情</a></th>
        </tr>
        </tfoot>
    </table>
    <footer>
		<?php
		$rel=get_class($this).'/conCommissionList-{page}?'.http_build_query($_GET,'','&');
		$this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
		?>
    </footer>
    <script type="text/javascript">
        ghhs("nav01","tr");
    </script>
</article>