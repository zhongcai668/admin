<?php
	$para=$_GET;
	
	// 默认取今天的数据
	if(isset($para['date'])){
		$date=strtotime($para['date']);
	}else{
		$date=strtotime('00:00');
	}
	// 取彩种信息
	$sql="select * from {$this->prename}type where id=?";
	$typeInfo=$this->getRow($sql, $this->type);
	
	// 取当天的开奖数据
	//$sql="select d.*, t.actionNo, t.actionTime from {$this->prename}data_time t left join {$this->prename}data d on right(d.number, 3)-0=t.actionNo where t.type=d.type and d.time between $date and $date+24*3600 and d.type={$this->type}";
	// 取当前彩种开奖时间表
	$sql="select * from {$this->prename}data_time where type={$this->type} order by actionTime";
	$times=$this->getPage($sql, $this->page, $this->pageSize);
	
	$dateString=date('Y-m-d ');
?>

<article class="module width_full">
  <input type="hidden" value="<?=$this->user['username']?>" />
  <header>
    <h3 class="tabs_involved">
      <?=$typeInfo['title']?>
      开奖数据
    </h3>
  </header>
  <table class="tablesorter" cellspacing="0">
    <thead>
      <tr>
        <th>彩种</th>
        <th>场次</th>
        <th>期数</th>
        <th>日期</th>
        <th>开奖数据</th>
       <!-- <th>状态</th>-->
        <th>开奖时间</th>
        <th>手动开奖<!--备注--></th>
      </tr>
    </thead>
    <tbody>
      <?php 
			$count=array();
				if($para['actionNo']) $times=array('data'=>array('id' =>'--'));
				$dateString=date('Y-m-d ', $date);
				
				foreach($times['data'] as $var){
					if($para['actionNo']){
						$data=$this->getRow("select * from {$this->prename}datas where type={$this->type} and number='{$para['actionNo']}'");
						$date=$data['time'];
						$var=array('actionNo'=>'--','actionTime'=>date('Y-m-d H:i:s', $date));
						$dateString='';
						
					}else{
	
						if($this->type == 14){					
							$number=1000+$var['actionNo'];
							$number=date('Ymd-', $date).substr($number,1);
							$sql="select * from {$this->prename}datas where type={$this->type} and number='$number'";
							$data=$this->getRow($sql);
							
						}
					}
				
			?>
      <tr>
        <td><?=$typeInfo['title']?></td>
        <td><?=$var['actionNo']?></td>
        <td><?=$this->ifs($data['number'], '--')?></td>
        <td><?=date('Y-m-d', $date)?></td>
        <td><?=$this->ifs($data['data'], '--')?></td>
        <!--<td><?=$this->iff($data['data'], '已开奖', '未开奖')?></td>-->
        <td><?=$dateString.$var['actionTime']?></td>
        <td><a href="/admin778899.php/kjdatas/add/<?=$this->type?>/<?=$var['actionNo']?>/<?=$dateString.$var['actionTime']?>" target="modal" width="340" title="添加开奖号码" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">添加</a>
      </td>
      </tr>
      <?php } ?>
    
    </tbody>
  </table>
  <footer>
    <?php
		//$rel=$args[0]['id'];
		if($para){
			$rel.='?'.http_build_query($para,'','&');
		}
		$rel=$this->controller.'/'.$this->action .'-{page}/'.$this->type.'?'.http_build_query($_GET,'','&');
		$this->display('inc/page.php', 0, $times['total'], $rel, 'dataPageAction');
	?>
  </footer>
</article>
