<?php
/**
 * Created by PhpStorm.
 * User: duzipeng
 * Date: 17/1/14
 * Time: 下午10:48
 */

ini_set('date.timezone', 'asia/shanghai');

const LOCAL_IP = '1921680169';

include_once dirname(__DIR__) .'/lib/core/DBAccess.class.php';

function db(){
    static $db = null;
    if(!is_null($db)) {
        return $db;
    }
    return $db = new DBAccess('mysql:host=127.0.0.1;dbname=boe2', 'root', 'root');
}


function logger($err){
    $file_name = pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_BASENAME);
    $err = '['. date('Y-m-d H:i:s') .'] ' . $err . "\n";
    file_put_contents(__DIR__ . '/logs/' . strtr($file_name, ['php' => 'log']), $err, FILE_APPEND);
    echo $err;
}


function getNumberByNo($type, $no, $date = null){
    if(empty($date)) $date = date('Ymd');
	if($type==1){
		$actionNo=date('Ymd', strtotime($date)).substr($no+1000,1);
		if($no==120) $actionNo=date('Ymd', strtotime($date)-24*3600).substr($no+1000,1);
	}else if($type==3){
		$actionNo=date('ymd', strtotime($date)).substr($no+100,1);
	}else if($type==11){
		$actionNo=date('Ymd-', strtotime($date)).substr($no+100,1);
	}else if($type==12){
		$actionNo=date('ymd', strtotime($date)).substr($no+100,1);
	}else if($type==5){
		$actionNo=date('Ymd-', strtotime($date)).substr($no+10000,1);
	}else if($type==25){
		$actionNo=date('md', strtotime($date)).substr($no+100,1);
	}else if($type==14 || $type==26 || $type==18 || $type==17 || $type==15){
		$actionNo=date('Ymd', strtotime($date)).substr($no+100,1);
	}else if($type==6 || $type==7){
		$actionNo=date('ymd', strtotime($date)).substr($no+100,1);
	}else{
		$actionNo = null;
	}
	return $actionNo;
}



function getPeriod1($period){
    return '20' . $period;
}

function getPeriod12($period){
    return $period;
}

function getData1($data){
    return implode(',', $data);
}

function getData6($data){
    foreach ($data as &$v) {
        $v = $v < 10? '0' .$v:$v;
    }
    return implode(',', $data);
}

function storeData($type, $period, $time, $data){
    if(getByNumber($type, $period)) {
        return false;
    }
    return db()->insert("insert into `ssc_data` (`type`, `time`, `number`, `data`) values (?,?,?,?)", [
        $type, $time, $period, $data
    ]);
}

function getDataTime($type){
    $times =  db()->getRows("select `actionTime`,`actionNo` from `ssc_data_time` where `type`=?", [
        $type
    ]);
    return array_column($times, 'actionNo', 'actionTime');
}

function getByNumber($type, $number){
    return  db()->getValue("select `data` from `ssc_data` where `type`=? AND `number`=?", [
        $type, $number
    ]);
}

function getBetInfo($type, $number){
    return  db()->getRow("select * from `ssc_bets` where `type`=? AND `actionNo`=? AND `status`=? LIMIT 1", [
        $type, $number , 0
    ]);
}

function getBetById($id){
	return  db()->getRow("select * from `ssc_bets` where `id`=?", [
		$id
	]);
}


function getNeedMate(){
	return  db()->getRows("select id from `ssc_bets` where `status`=? AND isDelete=0 AND kjTime<?", [
		0, time() - 50
	]);
}

function getTypeInfo($type){
	return  db()->getRow("select * from `ssc_type` where `id`=?  LIMIT 1", [
		$type
	]);
}

function handleBet($bet_info, $win_code, $func){
    $single_bonus  = $func($win_code, $bet_info['actionData']);
	$amount = $bet_info['beiShu'] * $single_bonus;
	sendBonus($bet_info, $amount, $win_code);
}

function sendBonus($bet_info, $amount, $win_code){
	db()->beginTransaction();
	try{
		updateBet($bet_info, $amount, $win_code);
		if($bet_info['hmEnable']) {
			$hmList = getHmList($bet_info['id']);
			foreach ($hmList as $hm) {
				$user_amount = round($hm['buy_volume'] / $bet_info['total_volume'] * $amount, 3);
				if($user_amount > 0) {
					handleBonus($hm['uid'], $user_amount, $bet_info);
					updateHm($hm['id'], $user_amount);
				}
			}
		} else {
			handleBonus($bet_info['uid'], $amount, $bet_info);
		}
		db()->commit();
	}catch (Exception $e) {
		db()->rollBack();
	}
}


function getHmList($bet_id){
    return db()->getRows("select   `id`,`uid`,`buy_volume` from `ssc_hm` where `bet_id`=? ", [
        $bet_id
    ]);
}


function updateBet($bet_info, $amount, $win_code, $status = 1){
    db()->updateRows('ssc_bets', [
        'bonus' => $amount,
        'lotteryNo' => $win_code,
        'zjCount' => 1,
        'status' => $status,
    ], '`id`=' . $bet_info['id']);
}


function updateHm($id, $amount){
    db()->updateRows('ssc_hm', [
        'bonus_amount' => $amount
    ], '`id`=' . $id);
}

function handleBonus($uid, $amount, $bet_info){
    addCoin(array(
        'uid'=>$uid,
        'type'=>$bet_info['type'],
        'liqType'=> 6,
        'info'=>($bet_info['hmEnable']?'合买':'代购'). getTypeName($bet_info['type']) .'中奖',
        'extfield0'=>$bet_info['id'],
        'extfield1'=>$bet_info['serializeId'],
        'coin'=>$amount,
    ));
}


function addCoin($log){
    if(!isset($log['uid'])) $log['uid']=$this->user['uid'];
    if(!isset($log['info'])) $log['info']='';
    if(!isset($log['coin'])) $log['coin']=0;
    if(!isset($log['type'])) $log['type']=0;
    if(!isset($log['fcoin'])) $log['fcoin']=0;
    if(!isset($log['extfield0'])) $log['extfield0']=0;
    if(!isset($log['extfield1'])) $log['extfield1']='';
    if(!isset($log['extfield2'])) $log['extfield2']='';
    $sql="call setCoin({$log['coin']}, {$log['fcoin']}, {$log['uid']}, {$log['liqType']}, {$log['type']}, '{$log['info']}', {$log['extfield0']}, '{$log['extfield1']}', '{$log['extfield2']}')";
    db()->insert($sql);
}


function getTypeName($type){
    $map = [
        1 => '重庆时时彩',
        3 => '黑龙江时时彩',
        5 => '急速分分彩',
        6 => '广东11选5',
        7 => '山东11选5',
        9 => '福彩3D',
        10 => '排列三',
        12 => '新疆时时彩',
        14 => '澳门五分彩',
        15 => '重庆11选5',
        16 => '江西11选5',
        18 => '重庆幸运农场',
        20 => '北京PK拾',
        59 => '重庆时时彩',
    ];
    return isset($map[$type])?$map[$type]:'';
}




function curlGet($url)
{
    $ch = curl_init($url); //初始化
    curl_setopt($ch, CURLOPT_HEADER, 0); //不返回header部分
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //返回字符串，而非直接输出
    curl_setopt($ch, CURLOPT_ENCODING, "gzip"); // gzip
    curl_setopt($ch, CURLOPT_TIMEOUT, 12);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}


function dd($v) {
    var_dump($v);die;
}

function trans($str){
	$rep = [
		'EMDE=M' => '01',
		'IMDI=M' => '02',
		'MMDM=M' => '03',
		'QMDQ=M' => '04',
		'UMDU=M' => '05',
		'YMDY=M' => '06',
		'cMDc=M' => '07',
		'gMDg=M' => '08',
		'kMDk=M' => '09',
		'AMTA=M' => '10',
		'EMTE=M' => '11',
		'**' => ',',
		"\t" => '',
		" " => '',
		"\r\n" => ''

	];
	return strtr($str, $rep);
}



function mateByNumber($type, $number){
	$win_code = getByNumber($type, $number);
	if(!$win_code) {
		logger('暂未获取到开奖数据');
		return;
	}
	$i = 0;
	while ($bet_info = getBetInfo($type, $number)) {
		$func = getPlayFunc($type, $bet_info['playedId']);
		if(is_null($func)) {
			updateBet($bet_info, 0, '', 2);
			continue;
		}
		handleBet($bet_info, $win_code, $func);
		$i++;
	}
	logger('开奖数据处理完毕,共' . $i .'条');
	exit;

}

function getPlayFunc($type, $playedId){
	switch ($type){
		case 1:
			switch ($playedId) {
				case 254:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(' ', $bet_no);
						$last = array_pop($number_arr);
						if(in_array($last, $bet_arr)) {
							return 18;
						} else {
							return 0;
						}
					};
					break;
				case 33:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$fifth = array_pop($number_arr);
						$fourth = array_pop($number_arr);
						if(strpos($bet_no, $fourth) !== false && strpos($bet_no, $fifth) !== false) {
							return 50;
						} else {
							return 0;
						}
					};
					break;
				case 27:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(',', $bet_no);
						if(count($bet_arr) != 2) {
							return 0;
						}
						$fifth = array_pop($number_arr);
						$fourth = array_pop($number_arr);
						if(strpos($bet_arr[0], $fourth) !== false && strpos($bet_arr[1], $fifth) !== false) {
							return 100;
						} else {
							return 0;
						}
					};
					break;
				case 12:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(',', $bet_no);
						if(count($bet_arr) != 3) {
							return 0;
						}
						$fifth = array_pop($number_arr);
						$fourth = array_pop($number_arr);
						$third = array_pop($number_arr);
						if(strpos($bet_arr[0], $fifth) !== false
							&& strpos($bet_arr[1], $fourth) !== false
							&& strpos($bet_arr[2], $third) !== false
						) {
							return 1000;
						} else {
							return 0;
						}
					};
					break;
				case 13:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(',', $bet_no);
						if(count($bet_arr) != 3) {
							return 0;
						}
						if($bet_arr[0] == $number_arr[2] && $bet_arr[1] == $number_arr[3] && $bet_arr[2] == $number_arr[4] ) {
							return 1000;
						} else {
							return 0;
						}
					};
					break;
				case 2:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(',', $bet_no);
						if(count($bet_arr) != 5) {
							return 0;
						}
						if(strpos($bet_arr[0], $number_arr[0]) !== false
							&& strpos($bet_arr[1], $number_arr[1]) !== false
							&& strpos($bet_arr[2], $number_arr[2]) !== false
							&& strpos($bet_arr[3], $number_arr[3]) !== false
							&& strpos($bet_arr[4], $number_arr[4]) !== false
						) {
							return 20000;
						} else {
							return 0;
						}
					};
					break;
				case 3:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(',', $bet_no);
						if(count($bet_arr) != 5) {
							return 0;
						}
						if($bet_arr[0] == $number_arr[0]
							&& $bet_arr[1] == $number_arr[1]
							&& $bet_arr[2] == $number_arr[2]
							&& $bet_arr[3] == $number_arr[3]
							&& $bet_arr[4] == $number_arr[4]
						) {
							return 20000;
						} else {
							return 0;
						}
					};
					break;
				case 43:
					return function ($numbers, $bet_no){
						$number_arr = explode(',', $numbers);
						$bet_arr = explode(',', $bet_no);
						if(count($bet_arr) != 2) {
							return 0;
						}
						$map = [
							'大' => [5,6,7,8,9],
							'小' => [0,1,2,3,4],
							'单' => [1,3,5,7,9],
							'双' => [0,2,4,6,8],
						];
						$bet0_arr = [];
						foreach (mb_str_split($bet_arr[0]) as $b) {
							$bet0_arr = array_merge($bet0_arr, $map[$b]);
						}
						$bet1_arr = [];
						foreach (mb_str_split($bet_arr[1]) as $b) {
							$bet1_arr = array_merge($bet1_arr, $map[$b]);
						}
						if(in_array($number_arr[3], $bet0_arr) && in_array($number_arr[4], $bet1_arr)) {
							return 4;
						} else {
							return 0;
						}
					};
					break;
			}
			break;
        case 6:
            switch ($playedId) {
                case 302:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        if(in_array($number_arr[0], $bet_arr)) {
                            return 18;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 53: //二直
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(',', $bet_no);
                        if(in_array($number_arr[0], explode(' ', $bet_arr[0])) &&
                            in_array($number_arr[1], explode(' ', $bet_arr[1]))) {
                            return 130;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 54:  //二组
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        if(in_array($number_arr[0],  $bet_arr) &&
                            in_array($number_arr[1], $bet_arr)) {
                            return 65;
                        } else {
                            return 0;
                        }
                    };
                    break;

                case 55: //三直
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(',', $bet_no);
                        if(in_array($number_arr[0], explode(' ', $bet_arr[0])) &&
                            in_array($number_arr[1], explode(' ', $bet_arr[1])) &&
                            in_array($number_arr[2], explode(' ', $bet_arr[3]))
                        ) {
                            return 1170;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 56:  //三组
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        if(in_array($number_arr[0],  $bet_arr) &&
                            in_array($number_arr[1], $bet_arr) &&
                            in_array($number_arr[3], $bet_arr)
                        ) {
                            return 195;
                        } else {
                            return 0;
                        }
                    };
                    break;

                case 45:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        if(in_array($number_arr[0], $bet_arr)) {
                            return 13;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 46:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 2) {
                            return 6;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 47:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 3) {
                            return 19;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 48:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 4) {
                            return 78;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 49:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 5) {
                            return 540;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 50:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 5) {
                            return 90;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 51:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 5) {
                            return 26;
                        } else {
                            return 0;
                        }
                    };
                    break;
                case 52:
                    return function ($numbers, $bet_no){
                        $number_arr = explode(',', $numbers);
                        $bet_arr = explode(' ', $bet_no);
                        $i = 0;
                        foreach ($bet_arr as $bet) {
                            if(in_array($bet, $number_arr)) $i++;
                        }
                        if($i >= 5) {
                            return 9;
                        } else {
                            return 0;
                        }
                    };
                    break;
                    break;


            }
	}
	logger($type . '未设置' . $playedId .'开奖函数');
	return null;
}

function mb_str_split( $string ) {
	return preg_split('/(?<!^)(?!$)/u', $string );
}

function formatNoToNumber1($no){
	return date('Ymd') . sprintf('%03d', $no);
}

function getNeedBuyBet(){
	$ip = LOCAL_IP;
	$time = time();
	$time_before = time() - 3;
	$sql="select id,available_volume,actionNum,mode,beiShu,total_volume from ssc_bets where actionIP={$ip} AND kjTime>{$time} AND available_volume>0 AND actionTime<{$time_before} LIMIT 1";
	return db()->getRow($sql);
}

function randUser(){
	$sql="select uid,username,nickname from ssc_members where xuni=1";
	$users =  db()->getRows($sql);
	$key = rand(0, count($users) - 1);
	return $users[$key];
}

function add_bet($type, $beiShu, $user){
	$total_volume =  rand(3, 6);
	$play_arr = [1 => ['playedGroup'=> 74, 'playedId'=>254], 6 => ['playedGroup'=> 80, 'playedId'=>302]] ;
	$actionData = $type == 1 ? '5 6 7 8 9': '01 02 03 04 05';
	$play_info = $play_arr[$type];
	db()->beginTransaction();
	try{
		list($kjTime, $actionNo) = getActionNo($type);
		$code = [
			'actionTime'=> time(),
			'actionNo'=> $actionNo,
			'orderId'=> '16' . time(),
			'kjTime'=> $kjTime,
			'actionIP'=> LOCAL_IP,
			'uid'=> $user['uid'],
			'username'=>$user['username'],
			'serializeId'=>uniqid(),
			'type'=> $type,
			'playedGroup'=> $play_info['playedGroup'],
			'playedId'=> $play_info['playedId'],
			'nickname'=>$user['nickname'],
			'hmEnable' => 1,
			'total_volume' => $total_volume,
			'available_volume' => $total_volume,
			'public_level' => 0,
			'zhuiHaoMode' => 1,
		];
		$code['wjorderId']= $type. $code['playedId'] . randomkeys(8-strlen($type.$code['playedId']));
		$code['actionData']= $actionData;
		$code['actionNum']= count(explode(' ', $actionData));
		$code['mode']= 2;
		$code['beiShu']= $beiShu;
		$amount=abs($code['actionNum']*$code['mode']*$code['beiShu']);
		db()->insertRow('ssc_bets', $code);
		$buy = rand(1, 2);
		$bet_id = db()->lastInsertId();
		hm($bet_id,  $buy, round($buy / $total_volume * $amount, 2), $user);
		db()->commit();
		return '投注成功';
	}catch(Exception $e){
		db()->rollBack();
		return $e->getMessage();
	}
}

function getActionNo($type){
	$type=intval($type);
	$time= time();
	$kjTime = $type == 1 ? 30 : 50;
	$atime = date('H:i:s', $time+$kjTime);
	$sql="select actionNo, actionTime from ssc_data_time where type=$type and actionTime>? order by actionTime limit 1";
	$return = db()->getRow($sql, $atime);
	$date = date('Ymd');
	if(!$return){
		$sql="select actionNo, actionTime from ssc_data_time where type=$type order by actionTime limit 1";
		$return =db()->getRow($sql, $atime);
		$date = date('Ymd', time() + 86400);
	}
	return [strtotime($return['actionTime']), getNumberByNo($type, $return['actionNo'], $date)];
}

function hm($bet_id, $buy_volume, $buy_amount, $user){
	db()->update("update ssc_bets set available_volume=available_volume-{$buy_volume} where id=?", $bet_id);
	$bet_info = getBetById($bet_id);
	if($bet_info['available_volume'] < 0){
		throw new \Exception('合买超标', 16556);
	}
	db()->insert("INSERT INTO ssc_hm (bet_id,buy_volume,buy_amount,uid,nickname,created_at)
	VALUES (?,?,?,?,?,?)", [$bet_id, $buy_volume, $buy_amount, $user['uid'], $user['username'], date('YmdHis')]);


}

function randomKeys($length)
{
	$key = "";
	$pattern='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	for($i=0;$i<$length;$i++)
	{
		$key .= $pattern{mt_rand(0,35)};
	}
	return $key;
}