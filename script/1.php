<?php

require_once __DIR__ . '/function.php';

action();

function action(){
	$type = 1;
	$times =  getDataTime($type);
	$time = date('H:i:00', time() - 60);
	if(!isset($times[$time])) {
		logger('不是开奖时段');
		exit();
	}
	for ($i = 0; $i < 30; $i++) {
		crawler($type, formatNoToNumber($times[$time]));
		sleep(5);
	}
}

function crawler($type, $number){
	if(getByNumber($type, $number)) {
		logger($number . '该次开奖已获取');
		exit();
	}
	$url = 'http://api.caipiao.163.com/missNumber_trend.html?product=caipiao_client&mobileType=iphone&ver=4.31&channel=appstore&apiVer=1.1&apiLevel=27&deviceId=080A-79C9-46E6-8B35-81E629&gameEn=ssc';
	$res = curlGet($url);
	$res_arr = json_decode($res, 1);
	if(!isset($res_arr['data']) || !$res_arr['data']) {
		logger( '返回数据格式错误');
	} else {
		logger( '获取到' . count($res_arr['data']) .'数据');
	}
	$time = time();
	foreach ($res_arr['data'] as $v) {
		if(!isset($v['winnerNumber']) ||  !$v['winnerNumber']) {
			logger($v['period'] . '暂无开奖数据'); continue;
		}
		$period = getPeriod1($v['period']);
		$ok = storeData($type, $period, $time, getData1($v['winnerNumber']));

		if($ok) {
			logger($v['period'] . '开奖数据已存储');
		}
		if($number == $period) {
			//exit();
		}
	}
}

function formatNoToNumber($no){
	return date('Ymd') . sprintf('%03d', $no);
}









