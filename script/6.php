<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();
function action(){
    $type = 6;
    $times = getDataTime($type);
    $time = date('H:i:00', time() - 60);
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }

    for ($i = 0; $i < 10; $i++) {
        $i > 0 && crawler($type, getNumberByNo($type, $times[$time]));
        sleep(7);
    }
}


function crawler($type, $number){
    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'https://www.gdlottery.cn/gdata/idx/11xuan5';
    $res = curlGet($url);

    $res_arr = json_decode($res, 1);
    if(!count($res_arr)) {
        logger( '返回数据格式错误');
    } else {
        logger( '获取到' . count($res_arr) .'数据');
    }
    $time = time();
    foreach ($res_arr as $v) {
        $period = trim($v['drawno']);
        $ok = storeData($type, $period, $time, implode(',',
            [fh($v['hm1']), fh($v['hm2']),fh($v['hm3']),fh($v['hm4']),fh($v['hm5']),]));
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}

function fh($v){
    return sprintf("%02d", $v);
}
