<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();

function action(){
    $type = 1;
    $times = getDataTime($type);
    $time = date('H:i:00', time());
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }
    for ($i = 0; $i < 25; $i++) {
        $i > 10 && crawler($type, formatNoToNumber($times[$time]));
        sleep(3);
    }
}


function crawler($type, $number){
    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'http://www.cqcp.net/game/ssc/';
    $res = curlGet($url);
    $html_dom = new ParserDom($res);
    $list = $html_dom->find('div#openlist',0);
    preg_match_all('/\d{10}-\d-\d-\d-\d/', $list->node->nodeValue, $res_arr);
    if(!isset($res_arr[0]) || !$res_arr[0]) {
        logger( '返回数据格式错误');
    } else {
        logger( '获取到' . count($res_arr[0]) .'数据');
    }
    $time = time();
    foreach ($res_arr[0] as $v) {
        if(18 != strlen($v)) {
            logger( '返回列表格式错误');
        }
        $period = getPeriod1(substr($v, 0, 9));
        $ok = storeData($type, $period, $time, strtr(substr($v, 9), ['-' => ',']));
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}

function formatNoToNumber($no){
    return date('Ymd') . sprintf('%03d', $no);
}









